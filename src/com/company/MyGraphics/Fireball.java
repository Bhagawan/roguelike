package com.company.MyGraphics;

import com.company.Resources.Spell;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
public class Fireball implements Spell {
    private BufferedImage bImage;
    private Image newI;
    private int x,y;


    private boolean cast = false;

    private int qq = 0, amount=0;
    public Fireball() {

        try {
            bImage = ImageIO.read(
                    new File("src/resources/Fireball_Spell.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public synchronized void cast(Point from, Point to, Component qqq) {
        to.x=to.x-25;
        to.y=to.y-25;
        cast=true;
        double dX = to.x - (from.x*50);
        double dY = to.y - (from.y*50);
        double cAngle = Math.atan2(dY, dX) + Math.PI/2;

        AffineTransform tx = new AffineTransform();
        System.out.println(Math.toDegrees(cAngle));
        tx.rotate(cAngle, bImage.getWidth()/2,bImage.getHeight()/2);
        AffineTransformOp op = new AffineTransformOp(tx,AffineTransformOp.TYPE_BILINEAR);
        BufferedImage rotated = op.filter(bImage,null);
        newI = (Image)rotated;
        double length = Math.sqrt(Math.pow(to.x - from.x*50, 2) + Math.pow(to.y - from.y*50, 2));
        double localX = (to.x - from.x*50) / length;
        double localY = (to.y - from.y*50) / length;
        int step = 5;
        amount = (int)(length / step);
        qq=1;
        Timer t = new Timer(1, null);
        t.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                x = (int)(from.x*50 + (localX*qq*step));
                y =(int) (from.y*50 + (localY*qq*step));
                qq+=1;
                if(qq>amount) {
                    cast = false;
                    t.stop();
                }
                qqq.repaint();
            }
        });
        t.start();
    }

    @Override
    public void draw(Graphics2D g2d, ImageObserver observer){
        g2d.drawImage(newI, x, y, observer);
    }



    @Override
    public boolean isCast() {
        return cast;
    }

}
