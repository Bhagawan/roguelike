package com.company.MyGraphics;

import java.awt.*;
import java.awt.image.ImageObserver;

public class Button {
    private Sprite imageDefault, imagePushed, imageSelected;
    private int x, y;
    private String name;
    public boolean pushed, selected;

    public Button(int x, int y, String name, String imageDef, String imagePush, String imageSel  ) {
        this.x = x;
        this.y = y;
        this.name = name;
        pushed = false;
        selected = false;
        this.imageDefault = new Sprite(imageDef);
        this.imagePushed = new Sprite(imagePush);
        this.imageSelected = new Sprite(imageSel);
    }

    public Button(int x, int y, String name, String path ) {
        this.x = x;
        this.y = y;
        this.name = name;
        pushed = false;
        selected = false;
        Sprite image = new Sprite(path);
        this.imageDefault = image;
        this.imagePushed = image;
        this.imageSelected = image;
    }

    public void draw(Graphics2D g2d, ImageObserver observer) {
        if (pushed) {
            g2d.drawImage(imagePushed.getImage(), x, y,observer);
        } else if (selected) {
            g2d.drawImage(imageSelected.getImage(), x, y,observer);
        } else g2d.drawImage(imageDefault.getImage(), x, y,observer);

    }

    public boolean onit(int x, int y) {
        return (x >= this.x && x <= this.x + this.imageDefault.getWidth() && y >= this.y && y <= this.y + this.imageDefault.getHeight());
    }

    public boolean onit(Point p) {
        return ((p.x >= this.x) && (p.x <= this.x + this.imageDefault.getWidth()) && (p.y >= this.y) && (p.y <= this.y + this.imageDefault.getHeight()));
    }

    public String getName(){
        return name;
    }


}
