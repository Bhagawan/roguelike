package com.company.MyGraphics;

import javax.swing.*;
import java.awt.*;

public class Sprite {
    protected int width;
    protected int height;
    protected Image image;

    public Sprite(){

    }

    public Sprite(String path){
        loadImage(path);
    }

    private void loadImage(String path) {
        ImageIcon ii = new ImageIcon(path);
        image = ii.getImage();
        width = image.getWidth(null);
        height = image.getHeight(null);
    }

    public Image getImage() {
        return image;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
