package com.company.Resources;

import java.awt.*;
import java.awt.image.ImageObserver;

public interface Spell {
    void cast(Point from, Point to, Component fd);
    boolean isCast();
    void draw(Graphics2D g2d, ImageObserver observer);
}
