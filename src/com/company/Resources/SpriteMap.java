package com.company.Resources;

import com.company.MyGraphics.Sprite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public final class SpriteMap {
    public static final Map<String, Sprite> spriteMap = new HashMap<String, Sprite>(){
        {
            put("floor",new Sprite("src/resources/floor.png"));
            put("wall",new Sprite("src/resources/wall.png"));
            put("lizard", new Sprite("src/resources/lizard.png"));
            put("exit", new Sprite("src/resources/exit.png"));
        }
    };
}
