package com.company.Resources;

import com.company.MyGraphics.Fireball;

import java.util.HashMap;
import java.util.Map;

public final class SpellBook {
    public static final Map<String,Spell> spellBook = new HashMap<String, Spell>() {
        {
            put("Fireball", new Fireball());
        }
    };
}
