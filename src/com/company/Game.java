package com.company;

import com.company.System.Callback;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

public class Game extends JFrame{
    private JPanel pan;
    private CardLayout screens = new CardLayout();
    private Game() {
        initUI();
    }


    private void initUI() {
        pan = new JPanel();
        pan.setLayout(screens);
        MenuScreen menuscreen = (new MenuScreen());
        menuscreen.callback = new Callback() {
            @Override
            public void swichto(String string) {switchLayout(string);}
        };
        GameScreen gameScreen = new GameScreen();
        gameScreen.callback = new Callback() {
            @Override
            public void swichto(String string) { switchLayout(string); }
        };
        pan.add(menuscreen,"MenuScreen");
        pan.add(gameScreen,"GameScreen");
        this.setContentPane(pan);

        pack();

        //setResizable(false);
        setTitle("Application");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

    }
    private void switchLayout(String string) {

        screens.show(pan, string);
    }


    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                Game game = new Game();
                game.setLocationRelativeTo(null);
                game.setVisible(true);
            }
        });
    }

}
