package com.company;

import com.company.Level.Character;
import com.company.Level.Level;
import com.company.Level.Monster;
import com.company.MyGraphics.Button;
import com.company.Resources.Spell;
import com.company.System.Callback;
import com.company.Resources.SpellBook;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class GameScreen extends Screen implements Runnable{
    public Callback callback = null;
    private Component drawComponent;
    private Level level;
    private Character hero;
    private Point helpP= new Point();
    private ArrayList<Monster> monstres = new ArrayList<>();
    private ArrayList<Button> buttons = new ArrayList<>();
    private String cast ="";

    public GameScreen() {
        setBackground(Color.BLACK);
        setFocusable(true);
        addKeyListener(new TAdapter());
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(java.awt.event.ComponentEvent e)
            {
                requestFocusInWindow();
            }
        });
        setPreferredSize(new Dimension(Constans.WIDTH,Constans.HEIGHT));
        hero = new Character();
        generateLevel();

        mouseListener = new MouseAdapter() {

            @Override
            public void mouseReleased(MouseEvent e) {
                SpellBook.spellBook.get(cast).cast(hero.getPoint(), e.getPoint(), drawComponent);
                wakeUp();
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                super.mouseMoved(e);
            }
        };
        addMouseListener(mouseListener);
        addMouseMotionListener(mouseListener);


    }

    private class TAdapter extends KeyAdapter {
        @Override
        public void keyReleased(KeyEvent e) {
        }

        @Override
        public synchronized void keyPressed(KeyEvent e) {
            int k = e.getKeyCode();
            switch ((k)){
                case KeyEvent.VK_UP:
                    if(level.isPassable(hero.getX(),hero.getY()-1)){
                        if( findMonster(hero.getX(),hero.getY()-1) != null) findMonster(hero.getX(),hero.getY()-1).loseHp(hero.getDamage());
                        else hero.moveUp();
                    }
                    wakeUp();
                    break;
                case KeyEvent.VK_DOWN:
                    if(level.isPassable(hero.getX(),hero.getY()+1)) {
                        if( findMonster(hero.getX(),hero.getY()+1) != null) findMonster(hero.getX(),hero.getY()+1).loseHp(hero.getDamage());
                        else hero.moveDown();
                    }
                    wakeUp();
                    break;
                case KeyEvent.VK_LEFT:
                    if(level.isPassable(hero.getX()-1,hero.getY())) {
                        if( findMonster(hero.getX()-1,hero.getY()) != null) findMonster(hero.getX()-1,hero.getY()).loseHp(hero.getDamage());
                        else hero.moveLeft();
                    }
                    wakeUp();
                    break;
                case KeyEvent.VK_RIGHT:
                    if(level.isPassable(hero.getX()+1,hero.getY())) {
                        if( findMonster(hero.getX()+1,hero.getY()) != null) findMonster(hero.getX()+1,hero.getY()).loseHp(hero.getDamage());
                        else hero.moveRight();
                    }
                    wakeUp();
                    break;
                case KeyEvent.VK_SPACE:
                    wakeUp();
                    break;

                case KeyEvent.VK_PERIOD:
                    if(e.isShiftDown() && hero.getPoint().equals(level.getExit())) generateLevel();
                    wakeUp();
                    break;
                case KeyEvent.VK_ENTER:
                    callback.swichto("MenuScreen");
                    break;
                case KeyEvent.VK_F:
                    cast ="Fireball";
                    break;
            }
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        drawComponent = this;
        something(g2d);
    }

    private void something(Graphics2D g2d) {
        level.drawLevel(g2d ,this);
        hero.drawHero(g2d, this);
        for(Monster monster:monstres) {
            monster.draw(g2d, this);
        }
        drawGUI(g2d);
        drawSpells(g2d);
    }

    private void drawGUI(Graphics2D g2d) {
        buttons.add(new Button(0, Constans.HEIGHT - 50, "Fireball", "src/resources/fireball.png"));
        for(Button b: buttons) {
            b.draw(g2d,this);
        }
    }
    private void drawSpells(Graphics2D g2d) {
        for(Spell spell:SpellBook.spellBook.values()) {
            if(spell.isCast()) spell.draw(g2d,this);
        }
    }

    @Override
    public void addNotify() {
        super.addNotify();
        Thread gameThread = new Thread(this);
        gameThread.start();
    }

    private void update(){
        updateMonstres();
        repaint();
    }

    private void updateMonstres() {
        for(int i = 0; i < monstres.size(); i++) {
            if (monstres.get(i).getHp() <=0) monstres.remove(i);
        }
        for(Monster m:monstres) {
            helpP = level.findPath(m.getPoint(), hero.getPoint());
            if (isEmpty(helpP) && helpP != hero.getPoint())
                m.moveTo(helpP);
        }
    }

    private void generateLevel() {
        level = new Level();
        monstres.clear();
        hero.moveHeroTo(level.getRandomSquare());
        for(int i=0; i<5; i++) {
            Point point = level.getRandomSquare();
            if(isEmpty(point)) {
                monstres.add(new Monster(point,"lizard"));
            }
            else i--;
        }
    }

    private synchronized void waitt() {
        try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }

    private boolean isEmpty(Point point) {
        for(Monster monster: monstres) {
            if(monster.getPoint().equals(point)) {
                System.out.println(monster.getPoint() +""+ point);
                return false;
            }
        }
        return !(new Point(hero.getX(), hero.getY()).equals(point));
    }

    private boolean isEmpty(int x, int y) {
        for(Monster monster: monstres) {
            if(monster.getX() == x && monster.getY() == y) {
                return false;
            }
        }
        return !(hero.getX() == x && hero.getY() == y);
    }

    private Monster findMonster(int x, int y) {
        for(Monster monster: monstres) {
            if(monster.getX() == x && monster.getY() == y) return monster;
        }
        return null;
    }


    private synchronized void wakeUp() {
        notifyAll();
    }

    @Override
    public synchronized void run() {
        while(true){
            update();
            waitt();

        }
    }
}
