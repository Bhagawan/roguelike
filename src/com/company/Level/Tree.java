package com.company.Level;

import java.awt.*;

public class Tree {
    public Room room = null;

    public Tree lbranch;
    public Tree rbranch;

    public Tree(Room room){
        this.room = room;
        lbranch = null;
        rbranch = null;
    }

    public Tree(){
        this.room = null;
        lbranch = null;
        rbranch = null;
    }

    public void drawTree(Graphics2D g2d) {
        if(room != null){
            room.draw(g2d);
        }
        if(lbranch != null) lbranch.drawTree(g2d);
        if(rbranch != null) rbranch.drawTree(g2d);
    }


}
