package com.company.Level;

import java.awt.*;
import java.awt.image.ImageObserver;

import com.company.Resources.SpriteMap;

import javax.swing.*;


public class Square {
    private int x;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    private int y;

    private String tile;
    public Square() {

    }
    public Square(int x, int y, String string) {
        this.x = x;
        this.y = y;
        this.tile = string;
    }

    public void drawSquare(Graphics2D g2d, ImageObserver observer) {
        g2d.drawImage(SpriteMap.spriteMap.get(tile).getImage(), x*50, y*50, observer);
    }

    public void setTile(String string){
        this.tile = string;
    }
    public boolean isPassable() {
        if(tile == "floor") return true;
        else return false;
    }

}
