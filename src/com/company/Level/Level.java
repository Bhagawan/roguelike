package com.company.Level;

import com.company.Resources.SpriteMap;

import java.awt.*;
import java.awt.image.ImageObserver;

public class Level {
    private Map map;
    private Point exit;



    public Level(){
        map = new Map();
        exit = map.findFreeSquare();
    }


    public void drawLevel(Graphics2D g2d , ImageObserver observer) {

        map.drawMap(g2d, observer);

        g2d.drawImage(SpriteMap.spriteMap.get("exit").getImage(), exit.x*50, exit.y*50, observer);
    }

    public Point getRandomSquare() {
        if (exit == null) return map.findFreeSquare();
        Point t = map.findFreeSquare();
        if (t.equals(exit)) return getRandomSquare();
        return t;
    }

    public Square getSquare(int x, int y) {
        return map.getSquare(x, y);
    }

    public boolean isPassable(int x, int y) {
        if(map.getSquare(x ,y)==null) return false;
        else return map.getSquare(x, y).isPassable();
    }

    public Point findPath(Point a, Point b) {
        return map.findPath(a, b);
    }

    public Point getExit() {
        return exit;
    }

}
