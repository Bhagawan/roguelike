package com.company.Level;

import com.company.Constans;

import java.awt.*;
import java.awt.image.ImageObserver;
import java.util.ArrayList;

public class Map {
    private final static int MAP_WIDTH = 27;
    private final static int MAP_HEIGHT = 15;
    private Square[][] map = new Square[MAP_WIDTH][MAP_HEIGHT];
    private Point[][] path = new Point[MAP_WIDTH][MAP_HEIGHT];
    private ArrayList<Point[]> corridors = new ArrayList<>();
    private Tree roomTree;

    public Map() {
        Room room = new Room(0,0, MAP_WIDTH,MAP_HEIGHT);
        roomTree = createTree(room,3);
        treeOnMap(roomTree);
        corridorsOnMap();
        for (int i=0 ; i < MAP_WIDTH; i++) {
            for (int j=0 ; j < MAP_HEIGHT; j++) path[i][j] = new Point(0, 0);
        }
    }

    private Tree createTree(Room room, int itamount) {
        Tree tree = new Tree();
        if(itamount == 0) {
            tree.room = room;
        }
        else {
            Room[] rooms = splitRoom(room);
            Point a = new Point((int)(rooms[0].getX() + (double)rooms[0].getWidth() / 2),(int)(rooms[0].getY() + (double)rooms[0].getHeight() / 2));
            Point b = new Point((int)(rooms[1].getX() + (double)rooms[1].getWidth() / 2),(int)(rooms[1].getY() + (double)rooms[1].getHeight() / 2));
            corridors.add(new Point[] {a,b} );
            tree.lbranch = createTree(rooms[0],itamount-1);
            tree.rbranch = createTree(rooms[1],itamount-1);
        }
        return tree;
    }

    private Room[] splitRoom(Room room) {
        Room[] rooms = new Room[2];

        if(Math.round(Math.random())==0) {
            // Vertical split
            rooms[0] = new Room( room.getX() , room.getY() ,(int)((1+Math.random()*(room.getWidth()-1))), room.getHeight());
            rooms[1] = new Room(room.getX()+rooms[0].getWidth() , room.getY(),room.getWidth()-rooms[0].getWidth(), room.getHeight());
            double a = rooms[0].getWidth()/(double)rooms[0].getHeight();
            double b = rooms[1].getWidth()/(double)rooms[1].getHeight();
            if(a<0.45||b<0.45) return splitRoom(room);
        }
        else {
            //Horizontal split
            rooms[0] = new Room(room.getX(), room.getY(),room.getWidth(), (int)(1 + (Math.random()*(room.getHeight()-1))));
            rooms[1] = new Room(room.getX(), (room.getY()+rooms[0].getHeight()), room.getWidth(),room.getHeight()-rooms[0].getHeight());
            double a = rooms[0].getHeight()/(double)rooms[0].getWidth();
            double b = rooms[1].getHeight()/(double)rooms[1].getWidth();
            if(a<0.45||b<0.45) return splitRoom(room);
        }
        return rooms;
    }

    private void treeOnMap(Tree tree) {
        if (tree.room != null ) fillMap(cutRoom(tree.room));
        if( tree.lbranch != null) treeOnMap(tree.lbranch);
        if( tree.rbranch != null) treeOnMap(tree.rbranch);
    }

    private Room cutRoom(Room room) {
        int x = room.getX() + (int)( Math.random() * ((double)room.getWidth() / 3));
        int y = room.getY() + (int)( Math.random() * ((double)room.getHeight() / 3));
        int width = room.getWidth() + (room.getX() - x) - (int)(Math.random() * ((double)room.getWidth() / 5));
        int height = room.getHeight() + (room.getY() - y) - (int)(Math.random() * ((double)room.getHeight() / 5));
        return new Room(x, y, width, height);
    }

    private void fillMap(Room room) {
        for(int i=room.getX(); i < room.getX() + room.getWidth(); i++)
            for (int j=room.getY(); j < room.getY() + room.getHeight(); j++) {
                if(i == room.getX() || j == room.getY() || i == (room.getX() + room.getWidth() - 1) || j == (room.getY() + room.getHeight() - 1)) {
                    map[i][j] = new Square(i,j,"wall");
                }
                else map[i][j] = new Square(i,j,"floor");
            }
    }

    private void corridorsOnMap() {
        for(Point[] points:corridors) {
            if(points[0].x == points[1].x) { //Вертикальный коридор (пора без кривых)
                for(int i = points[0].y; i < points[1].y; i++) {
                   dig(points[0].x,i);
                }
            }
            else { // горизонтальный
                for(int i = points[0].x; i < points[1].x; i++) {
                    dig(i,points[0].y);
                }
            }
        }
    }

    private void dig(int i, int j) {
        if(map[i][j] != null) map[i][j].setTile("floor");
        else map[i][j] = new Square(i,j,"floor");

        for( int x = -1; x<2; x++)
            for(int y = -1; y<2; y++) {
                if(map[i + x][j + y] == null) map[i + x][j + y] = new Square(i + x,j + y,"wall");
            }
    }

    public void drawMap(Graphics2D g2d , ImageObserver observer) {
        for(Square[] square:map) {
            for(Square squar:square) {
                if(squar != null) squar.drawSquare(g2d,observer);
            }
        }
    }

    public Point findFreeSquare() {
        int x = (int) (Math.random() * 27);
        int y = (int) (Math.random() * 15);
        if (map[x][y]!=null) {
            if(map[x][y].isPassable())return new Point(x, y);
        }
        return findFreeSquare();
    }

    public Square getSquare(int x, int y) {
        return map[x][y];
    }

    private boolean checkPoint(int x, int y) {
        if (y < 0 || y > MAP_HEIGHT ) return false;
        if (x < 0 || x > MAP_WIDTH ) return false;
        if (map[x][y] != null) return map[x][y].isPassable();
        return false;
    }

    private void setPath(int n, int x, int y) {
        path[x][y].x = n;
        if (checkPoint(x - 1, y)) {  //Проверим клетку слева  (4)
            if (path[x - 1][y].x > n + 1 || path[x - 1][y].x == 0) {
                path[x - 1][y].y = 2;
                setPath(n + 1, x - 1, y);
            }
        }
        if (checkPoint(x + 1, y)) {  //Проверим клетку справа  (2)
            if (path[x + 1][y].x > n + 1 || path[x + 1][y].x == 0) {
                path[x + 1][y].y = 4;
                setPath(n + 1, x + 1, y);
            }
        }
        if (checkPoint(x, y-1)) {  //Проверим клетку сверху  (1)
            if (path[x ][y-1].x > n + 1 || path[x ][y-1].x == 0) {
                path[x ][y-1].y = 3;
                setPath(n + 1, x , y-1);
            }
        }
        if (checkPoint(x, y+1)) {  //Проверим клетку снизу  (3)
            if (path[x ][y+1].x > n + 1 || path[x ][y+1].x == 0) {
                path[x ][y+1].y = 1;
                setPath(n + 1, x , y+1);
            }
        }

    }

    public Point findPath(Point a, Point b) {
        for (int i=0 ; i<MAP_WIDTH; i++) {
            for (int j=0 ; j<MAP_HEIGHT; j++) {
                path[i][j].x = 0;
                path[i][j].y = 0;
            }
        }
        setPath(0, b.x, b.y);
        Point p = new Point(a.x, a.y);
        switch (path[a.x][a.y].y) {
            case 1:
                p.y-=1;
                break;
            case 2:
                p.x+=1;
                break;
            case 3:
                p.y+=1;
                break;
            case 4:
                p.x-=1;
                break;
        }
        return p;
    }

}
