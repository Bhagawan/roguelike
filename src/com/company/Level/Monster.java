package com.company.Level;

import com.company.Resources.SpriteMap;

import java.awt.*;
import java.awt.image.ImageObserver;

public class Monster {
    private int x;
    private int y;
    private String tag;
    private int hp = 50;

    public Monster(int x, int y, String tag) {
        this.x = x;
        this.y = y;
        this.tag = tag;
    }

    public Monster(Point point, String tag) {
        this.x = point.x;
        this.y = point.y;
        this.tag = tag;
    }
    public void draw(Graphics2D g2d, ImageObserver observer) {
        g2d.drawImage(SpriteMap.spriteMap.get(tag).getImage(), x*50, y*50, observer);
        g2d.setColor(Color.green);
        int life =  (int)(((float)50/100)*(hp/((float)50/100)));
        g2d.fillRect(x*50, y*50-3,life,3);
        if(life != 50) {
            g2d.setColor(Color.RED);
            g2d.fillRect(x*50+life, y*50-3,50-life,3);
        }

    }

    public void moveTo(Point a) {
        System.out.println("from: " + this.x + " " + this.y + " to: " + a);
        this.x = a.x;
        this.y = a.y;
    }


    public void loseHp(int x) {
        this.hp -= x;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getHp() {
        return hp;
    }

    public Point getPoint() {
        return new Point(x, y);
    }
}
