package com.company.Level;

import java.awt.*;

public class Room {
    private int x;
    private int y;
    private int height;
    private int width;

    public Room(int x,int y,int width,int height){
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
    }

    public void draw(Graphics2D g2d){
        g2d.setColor(Color.ORANGE);
        g2d.drawRect(x,y,width,height);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
}
