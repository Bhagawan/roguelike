package com.company.Level;

import javax.swing.*;
import java.awt.*;
import java.awt.image.ImageObserver;

public class Character {
    private Point point;
    private Image image;
    private int damage = 20;

    public Character(){
        ImageIcon ii = new ImageIcon("src/resources/Hero.png");
        image = ii.getImage();
    }

    public Character(Point point){
        this.point = point;
        ImageIcon ii = new ImageIcon("src/resources/Hero.png");
        image = ii.getImage();
    }

    public void drawHero(Graphics2D g2d, ImageObserver observer) {
        g2d.drawImage(image, point.x*50, point.y*50, observer);
    }

    public void moveHeroTo(int x, int y) {
        this.point.x = x;
        this.point.y = y;
    }

    public void moveHeroTo(Point p) {
        this.point = p;
    }


    public void moveUp(){
        this.point.y -= 1;
    }
    public void moveDown(){
        this.point.y += 1;
    }
    public void moveLeft(){
        this.point.x -= 1;
    }
    public void moveRight(){
        this.point.x += 1;
    }

    public Point getPoint() {
        return point;
    }

    public int getX() {
        return point.x;
    }

    public int getY() {
        return point.y;
    }

    public int getDamage() {
        return damage;
    }
}
