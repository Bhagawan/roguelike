package com.company;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.ImageObserver;
import java.util.ArrayList;
import com.company.MyGraphics.Button;
import com.company.System.Callback;

public class MenuScreen extends Screen{
    public Callback callback = null;
    private ArrayList<Button> buttons = new ArrayList<>();

    public MenuScreen() {
        setBackground(Color.BLACK);
        setFocusable(true);
        addKeyListener(new MenuScreen.TAdapter());
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(java.awt.event.ComponentEvent e)
            {
                requestFocusInWindow();
            }
        });
        setPreferredSize(new Dimension(Constans.WIDTH,Constans.HEIGHT));
        buttons.add(new Button(450, 100, "Start", "src/resources/Start.png","src/resources/Start_selected.png","src/resources/Start_pushed.png"));
        buttons.add(new Button(550,500, "Exit","src/resources/exitButton.png","src/resources/exitButton_pushed.png","src/resources/exitButton_pushed.png"));

        mouseListener = new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                for(Button b: buttons){
                    b.pushed = b.onit(e.getPoint());
                    repaint();
                }
            }
            @Override
            public void mouseMoved(MouseEvent e) {
                for(Button b: buttons){
                    b.selected = b.onit(e.getPoint());
                    repaint();
                }
            }
            @Override
            public void mouseReleased(MouseEvent e) {
                for(Button b: buttons){
                    b.pushed = b.onit(e.getPoint());
                    repaint();
                    if(b.onit(e.getPoint())){
                        switch (b.getName()) {
                            case "Start":
                                callback.swichto("GameScreen");
                                break;
                            case "Exit":
                                System.exit(0);
                                break;
                        }
                    }
                }
            }
        };
        addMouseListener(mouseListener);
        addMouseMotionListener(mouseListener);
    }

    private class TAdapter extends KeyAdapter {
        @Override
        public void keyReleased(KeyEvent e) {
            int k = e.getKeyCode();
            switch (k) {
                case KeyEvent.VK_SPACE:
                    setBackground((Color.BLACK));
                    break;
            }
        }

        @Override
        public void keyPressed(KeyEvent e) {
            int k = e.getKeyCode();
            switch (k) {
                case KeyEvent.VK_SPACE:
                    setBackground((Color.RED));
                    break;
                case KeyEvent.VK_ENTER:
                    callback.swichto("GameScreen");
                    break;
            }
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        DrawMenu(g2d,this);
    }

    private void DrawMenu(Graphics2D g2d, ImageObserver observer) {
        for(Button button : buttons ) {
            button.draw(g2d, observer);
        }

    }

}
